package com.pandri.error;

public class NotFound extends RuntimeException{
	public NotFound(Long id) {
		super("Object not found: " + id);
	}
}
