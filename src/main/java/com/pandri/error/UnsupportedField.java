package com.pandri.error;

import java.util.Set;

public class UnsupportedField extends RuntimeException {
	public UnsupportedField(Set<String> keys) {
		super("Field " + keys.toString() + " update is not allow");
	}
}
