package com.pandri.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pandri.model.Customer;

@Repository("customerDao")
public interface CustomerDao extends JpaRepository<Customer, Long> {

}
