package com.pandri.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pandri.dao.CustomerDao;
import com.pandri.error.NotFound;
import com.pandri.model.Customer;

@RestController
@RequestMapping("/commonwealth/customer")
public class CustomerController {

	@Autowired
	private CustomerDao customerDao;

	@GetMapping("/")
	public ResponseEntity<List<Customer>> viewData() {
		return ResponseEntity.ok(customerDao.findAll());
	}

	@GetMapping("/{id}")
	public Customer getCustomerById(@PathVariable Long id) {
		return customerDao.findById(id).orElseThrow(() -> new NotFound(id));
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/")
	public ResponseEntity saveOrUpdate(@RequestBody Map<String, Object> mapInput) {
		Customer customer = null;
		try {
			if (mapInput.get("id") != null && !mapInput.get("id").toString().isEmpty()) {
				Long id = Long.parseLong(mapInput.get("id").toString());
				customer = customerDao.findById(id).get();
			} else {
				customer = new Customer();
			}

			customer.setName(mapInput.get("name").toString());
			customer.setPhone(mapInput.get("phone").toString());

			customerDao.save(customer);
		} catch (Exception e) {
			String notifError = "error save or update";
			System.err.println(notifError);
			return ResponseEntity.ok(notifError);
		}
		return ResponseEntity.ok(customer);
	}

	@SuppressWarnings("rawtypes")
	@GetMapping("/delete/{id}")
	public ResponseEntity deleteById(@PathVariable Long id) {
		try {
			Customer customer = customerDao.findById(id).get();
			customerDao.delete(customer);
			return ResponseEntity.ok("Success");
		} catch (Exception e) {
			String notifError = "Error delete customer";
			System.err.println(notifError);
			return ResponseEntity.ok(notifError);
		}

	}
}
